//
//  TweetData.swift
//  Birdy
//
//  Created by Student on 11.01.2024..
//

import Foundation

class TweetData: ObservableObject {
    
    @Published var tweets: [Tweet] = [
        Tweet(
            username: "T",
            content: "Hello",
            isFavorite: true),
        Tweet(
            username: "A",
            content: "Ty",
            isFavorite: true),
        Tweet(
            username: "J",
            content: "Nice",
            isFavorite: true)
    ]
}
