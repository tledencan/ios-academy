//
//  SearchView.swift
//  Birdy
//
//  Created by Student on 11.01.2024..
//

import SwiftUI

struct SearchView: View {
    
    @EnvironmentObject var tweetData: TweetData
    
    @State var query = ""
    
    var foundedTweets : [Tweet] {
        if query.isEmpty {
            return tweetData.tweets
        }
        else {
            return tweetData.tweets.filter {tweet in
                return tweet.username.contains(query)
                    || tweet.content.contains(query)
            }
        }
    }
    
    var body: some View {
        
        VStack {
            TextField("Search", text: $query)
                        .autocapitalization(.none)
                        .padding(.leading, 10)
                        .padding(.trailing, 10)
                        .padding(.top, 30)
                        .padding(.bottom, 10)
                        .multilineTextAlignment(.center)
                        .textFieldStyle(.roundedBorder)
                    
            List(Binding.constant(foundedTweets)) { tweet in
                        TweetRow(tweet: tweet)
                    }
                    .listStyle(.plain)
                    
                    Spacer()
        }
        
    }
}

#Preview {
    SearchView()
        .environmentObject(TweetData())
}
