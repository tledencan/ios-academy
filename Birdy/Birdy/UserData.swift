//
//  UserData.swift
//  Birdy
//
//  Created by Student on 11.01.2024..
//

import Foundation
import Combine

class UserData: ObservableObject {
    @Published var username = "Tia"
    @Published var imageName = "photo"
    @Published var myTweetsIds: [String] = []
    @Published var likedTweetsIds: [String] = []
}
