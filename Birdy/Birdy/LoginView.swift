//
//  LoginView.swift
//  Birdy
//
//  Created by Student on 19.12.2023..
//

import SwiftUI

struct LoginView: View {
    
    @Binding var username : String
    @Binding var isPresented: Bool
    
    var body: some View {
        VStack {
            Image("bfb")
                .resizable()
                .frame(width: 55, height: 55)
                .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
            
            Text("Birdy")
                .font(.title)
                .padding()
            
            TextField("Username", text: $username)
                .padding(.leading, 100)
                .padding(.trailing, 100)
                .padding(.bottom, 10)
                .multilineTextAlignment(.center)
                .textFieldStyle(.roundedBorder)
            
            Button(action:{isPresented = false}) {
                Text("Log in")
                    .foregroundColor(.black)
            }
            .frame(width:75, height: 30)
            .background(Color.green.opacity(0.5))
            .cornerRadius(15.0)
            .disabled(username.isEmpty)
        
            
        }.padding()
        
    }
}

#Preview {
    LoginView(username: Binding.constant(""), isPresented:Binding.constant(true))
}
