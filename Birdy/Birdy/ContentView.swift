//
//  ContentView.swift
//  Birdy
//
//  Created by Student on 13.12.2023..
//

import SwiftUI


struct ContentView: View {
    
    @State var content: String = ""
    @State var isPresented: Bool = false
    @State var username: String = ""
    
    @EnvironmentObject var tweetData: TweetData
    @EnvironmentObject var userData: UserData
    
    var body: some View {
        VStack {
            HStack {
                Text("Birdy")
                    .font(.title)
                    .bold()
                Spacer()
                if username.isEmpty {
                    Button(action: {isPresented = true}, label: {
                        Text("Login")
                    })
                } else {
                    Button(action: {
                        username = ""
                    }, label: {
                        Text("Log out")
                    })
                    
                }
            }
            
            List($tweetData.tweets) { tweet in
                TweetRow(tweet: tweet)
            }
            .listStyle(.plain)
            
            Spacer()
            
            if !username.isEmpty {
                
            HStack {
                TextField("Content", text: $content)
                Button(action:{
                    tweetData.tweets.append(
                        Tweet(
                            username: username,
                            content: content,
                            isFavorite: false))
                    content = ""
                }){
                    Text("Add tweet")
                }
                .disabled(content.isEmpty)
                    
                }
            }
            
        }
        .padding()
        .sheet(isPresented: $isPresented) {
            LoginView(username: $username, isPresented: $isPresented)
        }
    }
}

#Preview {
    ContentView()
        .environmentObject(TweetData())
        .environmentObject(UserData())
}
