//
//  TweetRow.swift
//  Birdy
//
//  Created by Student on 19.12.2023..
//

import SwiftUI

struct TweetRow: View {
    
    @Binding var tweet: Tweet
    @EnvironmentObject var userData: UserData
    
    var body: some View {
        HStack{
            
            Image("bfb")
                .resizable()
                .frame(width: 55, height: 55)
                .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
            
            VStack (alignment:.leading){
                Text(tweet.username)
                    .foregroundColor(.gray)
                    .font(.subheadline)
                    .padding(.bottom, 1)
                
                Text(tweet.content)
                
                Text(tweet.date, style: .relative)
                    .foregroundColor(.gray)
                    .font(.caption)
                
                
            }.padding(.leading)
            Spacer()
            Button(action:{
                tweet.isFavorite.toggle()
                if tweet.isFavorite {
                    userData.likedTweetsIds.append(tweet.id)
                }
                else {
                    if let ind = userData.likedTweetsIds.firstIndex(of: tweet.id){
                        userData.likedTweetsIds.remove(at: ind)
                    }
                                    }
            }) {
                if tweet.isFavorite{
                    Image(systemName: "heart.fill")
                        .foregroundColor(.red)

                }
                else {
                    Image(systemName: "heart")
                        .foregroundColor(.red)

                }
            }
        }
    }
}

#Preview {
    TweetRow(tweet: 
        Binding.constant(
            Tweet(username: "Tia", content:"Ty", isFavorite: true)
        )
    ).environmentObject(UserData())
}
