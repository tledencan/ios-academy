//
//  BirdyApp.swift
//  Birdy
//
//  Created by Student on 13.12.2023..
//

import SwiftUI

@main
struct BirdyApp: App {
    
    @StateObject var tweetData = TweetData()
    @StateObject var userData = UserData()
    
    var body: some Scene {
        WindowGroup {
            TabView {
                ContentView()
                    .tabItem {
                        Label("Tweets", systemImage: "list.bullet.circle")
                    }
                SearchView()
                    .tabItem {
                        Label("Search", systemImage: "magnifyingglass.circle")
                    }
            }
            .environmentObject(tweetData)
            .environmentObject(userData)
        }
    }
}
